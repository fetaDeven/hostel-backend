import { Request, Response } from "express";
import db from "../database/models/index";
import httpResponse from "../helpers/httpResponse.helper";

const http = new httpResponse();

class RoomsController {
  async getFloorRooms(req: Request, res: Response) {
    try {
      const { floorId } = req.params;
      const rooms = await db.Rooms.findAll({
        where: { floor_id: floorId },
      });
      http.setSuccess(200, "Rooms retrieved", rooms);
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get rooms", {
        message: message,
      });
      http.send(res);
    }
  }

  async createRoom(req: Request, res: Response) {
    try {
      const { body } = req;

      body.created_by = req.headers.id;
      const createdRoom = await db.Rooms.create(body);

      http.setSuccess(201, "Room created", { ...createdRoom?.dataValues });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to create room", {
        message: message,
      });
      http.send(res);
    }
  }

  async updateRoom(req: Request, res: Response) {
    try {
      const { user, body, params } = req;
      const updatedRoom = await db.Rooms.update(body, {
        where: { id: params.id },
      });
      http.setSuccess(201, "Room updated", { ...updatedRoom?.dataValues });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to update room", {
        message: message,
      });
      http.send(res);
    }
  }

  async getRoomDetails(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const room = await db.Rooms.findOne({
        where: { id: id },
      });
      http.setSuccess(200, "Room retrieved", room);
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get room", {
        message: message,
      });
      http.send(res);
    }
  }
  deleteRoom(req: Request, res: Response) {
    try {
      const { params } = req;
      const deletedRoom = db.Rooms.destroy({
        where: { id: params.id },
      });
      http.setSuccess(200, "Room deleted", deletedRoom);
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to delete room", {
        message: message,
      });
      http.send(res);
    }
  }
}

export default RoomsController;
