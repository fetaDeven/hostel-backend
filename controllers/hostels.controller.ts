import { Request, Response } from "express";
import { hostelObject } from "../config/types";
import db from "../database/models/index";
import httpResponse from "../helpers/httpResponse.helper";
import { Op } from "sequelize";
import path from "path";

const http = new httpResponse();

class HostelController {
  async getHostels(req: Request, res: Response) {
    try {
      const { id } = req.headers;
      const availableHostels = await db.Hostels.findAll({});
      const userHostels = await db.Hostels.findAll({
        where: { created_by: id },
      });

      const user = await db.User.findOne({
        where: { id: id },
      });

      const room = await db.Booking.findOne({
        where: { user_id: id },
        order: [["created_at", "DESC"]],
        include: [
          { association: db.Booking.room, attributes: ["name"] },
          { association: db.Booking.hostel, attributes: ["name"] },
        ],
      });

      const basedOnCourse = await db.Hostels.findAll({
        include: {
          association: db.Hostels.hostelStudents,
          where: {
            type: "Student",
            course: {
              [Op.iLike]: `%${user.course?.toLocaleString().trim()}%`,
            },
          },
        },
      });

      const basedOnUniversity = await db.Hostels.findAll({
        include: {
          association: db.Hostels.hostelStudents,
          where: {
            type: "Student",
            course: {
              [Op.iLike]: `%${user.university?.toLocaleString().trim()}%`,
            },
          },
        },
      });

      const suggestions = [...basedOnCourse, ...basedOnUniversity];

      http.setSuccess(200, "Hostels retrieved", {
        availableHostels,
        userHostels,
        room,
        user,
        suggestions,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get hostels", {
        message: message,
      });
      http.send(res);
    }
  }

  async getHostelDetails(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const hostel = await db.Hostels.findOne({ where: { id: id } });

      const floors = await db.Floors.findAll({
        where: { hostel_id: id },
        include: {
          association: db.Floors.rooms,
        },
      });

      const rooms: any = [];

      floors.forEach((floor: any) => {
        floor?.rooms?.forEach((room: any) => {
          rooms.push({
            ...room?.dataValues,
            floor: floor.name,
          });
        });
      });

      const bookings = await db.Booking.findAll({
        where: { hostel_id: id },
        include: [
          { association: db.Booking.room },
          {
            association: db.Booking.student,
          },
        ],
      });

      http.setSuccess(200, "Hostel retrieved", {
        ...hostel?.dataValues,
        rooms,
        bookings,
        floors,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get hostel", {
        message: message,
      });
      http.send(res);
    }
  }

  async createHostel(req: Request, res: Response) {
    try {
      const { body } = req;

      const { id } = req.headers;
      body.created_by = id;
      const createdHostel = await db.Hostels.create(body);

      http.setSuccess(201, "Hostel created", { ...createdHostel?.dataValues });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to create hostel", {
        message: message,
      });
      http.send(res);
    }
  }

  async searchHostels(req: Request, res: Response) {
    try {
      const { search, searchBy } = req.query;
      let searchResult: hostelObject | hostelObject[] = [];

      if (searchBy === "name" || searchBy === "location") {
        searchResult = await db.Hostels.findAll({
          where: {
            [searchBy]: {
              [Op.iLike]: `%${search?.toLocaleString().trim()}%`,
            },
          },
        });
      } else if (searchBy === "course") {
        searchResult = await db.Hostels.findAll({
          include: {
            association: db.Hostels.hostelStudents,
            where: {
              type: "Student",
              course: {
                [Op.iLike]: `%${search?.toLocaleString().trim()}%`,
              },
            },
          },
        });
      } else if (searchBy === "university") {
        searchResult = await db.Hostels.findAll({
          include: {
            association: db.Hostels.hostelStudents,
            where: {
              type: "Student",
              university: {
                [Op.iLike]: `%${search?.toLocaleString().trim()}%`,
              },
            },
          },
        });
      }

      // find hostel by user

      http.setSuccess(200, "Hostels retrieved", { search: searchResult });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get hostels", {
        message: message,
      });
      http.send(res);
    }
  }

  static async uploadImages(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { file, body } = req;

      if (!file) {
        throw new Error("Please upload an image");
      }

      const url = path.join(`/images/${file.filename}`);

      const image = await db.Images.create({
        owner_id: id,
        image: url,
        type: body.type,
      });

      http.setSuccess(200, "Hostel updated", {
        ...image.dataValues,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to upload images", {
        message: message,
      });
      http.send(res);
    }
  }
}

export default HostelController;
