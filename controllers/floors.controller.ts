import { Request, Response } from "express";
import db from "../database/models/index";
import httpResponse from "../helpers/httpResponse.helper";

const http = new httpResponse();

class FloorsController {
  async createFloor(req: Request, res: Response) {
    try {
      const { body } = req;
      body.created_by = req.params.id;
      const createdFloor = await db.Floors.create(body);
      http.setSuccess(201, "Floor created", { ...createdFloor?.dataValues });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to create floor", {
        message: message,
      });
      http.send(res);
    }
  }
  async updateFloor(req: Request, res: Response) {
    try {
      const { body, params } = req;
      const updatedFloor = await db.Floors.update(body, {
        where: { id: params.id },
      });
      http.setSuccess(201, "Floor updated", { ...updatedFloor?.dataValues });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to update floor", {
        message: message,
      });
      http.send(res);
    }
  }
  async getFloors(req: Request, res: Response) {
    try {
      const { hostelId } = req.params;
      console.log(hostelId);
      const floors = await db.Floors.findAll({
        where: { hostel_id: hostelId },
      });
      http.setSuccess(200, "Floors retrieved", { floors });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get floors", {
        message: message,
      });
      http.send(res);
    }
  }
  async deleteFloor(req: Request, res: Response) {
    try {
      const { params } = req;
      const deletedFloor = await db.Floors.destroy({
        where: { id: params.id },
      });
      http.setSuccess(200, "Floor deleted", deletedFloor);
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to delete floor", {
        message: message,
      });
      http.send(res);
    }
  }

  async getFloorDetails(req: Request, res: Response) {
    try {
      const { params } = req;
      const floor = await db.Floors.findOne({ where: { id: params.id } });
      const hostel = await db.Hostels.findOne({
        where: { id: floor?.hostel_id },
      });
      const rooms = await db.Rooms.findAll({ where: { floor_id: params.id } });
      http.setSuccess(200, "Floor retrieved", {
        ...floor?.dataValues,
        rooms,
        hostel,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get floor", {
        message: message,
      });
      http.send(res);
    }
  }
}

export default FloorsController;
