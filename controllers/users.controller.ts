import { Request, Response } from "express";
import db from "../database/models/index";
import httpResponse from "../helpers/httpResponse.helper";

const http = new httpResponse();

class UsersController {
  async loginUser(req: Request, res: Response) {
    try {
      const { body } = req;
      const user = await db.User.findOne({
        where: { username: body.username.trim() },
        attributes: {
          include: ["password"],
        },
      });
      if (!user) {
        http.setError(400, "User not found", {
          message: "User not found",
        });
        http.send(res);
      } else {
        if (body.password.trim() !== user.password) {
          http.setError(400, "Invalid password", {
            message: "Invalid password",
          });
          http.send(res);
        } else {
          http.setSuccess(200, "User logged in", { user });
          http.send(res);
        }
      }
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to login user", {
        message: message,
      });

      http.send(res);
    }
  }
  async getUsers(req: Request, res: Response) {
    try {
      const users = await db.User.findAll({});

      http.setSuccess(200, "Users retrieved", { users });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get users", {
        message: message,
      });
      http.send(res);
    }
  }

  async createUser(req: Request, res: Response) {
    try {
      const { body } = req;

      const createdUser = await db.User.create(body);

      http.setSuccess(201, "User created", { user: createdUser });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to create user", {
        message: message,
      });

      http.send(res);
    }
  }

  async verifyUser(req: Request, res: Response) {
    try {
      const { user } = req;

      const verifiedUser = await db.Users.findOne({ where: { id: user.id } });
      http.setSuccess(201, "User created", { ...verifiedUser?.dataValues });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to create user", {
        message: message,
      });

      http.send(res);
    }
  }
}

export default UsersController;
