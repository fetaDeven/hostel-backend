import { Request, Response } from "express";
import db from "../database/models/index";
import httpResponse from "../helpers/httpResponse.helper";

const http = new httpResponse();

class BookingController {
  async getBooking(req: Request, res: Response) {
    try {
      const { hostelId } = req.query;
      const booking = await db.Booking.findAll({
        where: { hostel_id: hostelId },
        include: [
          { association: db.Booking.room },
          {
            association: db.Booking.student,
          },
        ],
      });
      http.setSuccess(200, "Booking retrieved", { booking });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get Booking", {
        message: message,
      });

      http.send(res);
    }
  }

  async createBooking(req: Request, res: Response) {
    try {
      const { body } = req;

      body.created_by = req.headers.id;
      const createdBooking = await db.Booking.create(body);

      const room = await db.Rooms.update(
        { booked: true },
        { where: { id: body.room_id } }
      );

      http.setSuccess(201, "Booking created", {
        ...createdBooking?.dataValues,
        room,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to create booking", {
        message: message,
      });

      http.send(res);
    }
  }
  async updateBooking(req: Request, res: Response) {
    try {
      const { body, params } = req;
      const updatedBooking = await db.Booking.update(body, {
        where: { id: params.id },
      });
      http.setSuccess(201, "Booking updated", {
        ...updatedBooking?.dataValues,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to update booking", {
        message: message,
      });

      http.send(res);
    }
  }
  async deleteBooking(req: Request, res: Response) {
    try {
      const { params } = req;
      const deletedBooking = await db.Booking.destroy({
        where: { id: params.id },
      });
      http.setSuccess(201, "Booking deleted", {
        ...deletedBooking?.dataValues,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to delete booking", {
        message: message,
      });

      http.send(res);
    }
  }
  async getMyBooking(req: Request, res: Response) {
    try {
      const { id } = req.query;
      const myBooking = await db.Booking.findAll({
        where: { user_id: id },
        include: [
          { association: db.Booking.room },
          {
            association: db.Booking.hostel,
          },
        ],
      });
      http.setSuccess(200, "Booking retrieved", { myBooking });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get Booking", {
        message: message,
      });

      http.send(res);
    }
  }

  async acceptBooking(req: Request, res: Response) {
    try {
      const { body, params } = req;
      const booking = await db.Booking.findOne({ where: { id: params.id } });
      const room = await db.Rooms.findOne({ where: { id: booking.room_id } });

      if (room.occupants >= room.total_occupants)
        throw new Error("Room is full");

      const updatedBooking = await db.Booking.update(
        { status: "Accepted", end_date: body.end_date },
        { where: { id: params.id } }
      );
      const user = db.User.update(
        { current_hostel: booking.hostel_id },
        { where: { id: body.user_id } }
      );

      const updatedRoom = await db.Rooms.update(
        { occupants: room.occupants + 1 },
        { where: { id: booking.room_id } }
      );

      http.setSuccess(201, "Booking Accepted", {
        ...updatedBooking?.dataValues,
        user,
        updatedRoom,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to update booking", {
        message: message,
      });

      http.send(res);
    }
  }

  async acceptPayment(req: Request, res: Response) {
    try {
      const { params, body } = req;
      const updatedBooking = await db.Booking.update(
        { paid: true, status: `Accepted-${body.amount}` },
        {
          where: { id: params.id },
        }
      );
      http.setSuccess(201, "Payment accepted", {
        ...updatedBooking?.dataValues,
      });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to update booking", {
        message: message,
      });

      http.send(res);
    }
  }

  async getAcceptedBookings(req: Request, res: Response) {
    try {
      const { hostelId } = req.query;
      const booking = await db.Booking.findAll({
        where: { hostel_id: hostelId, status: "Accepted", paid: false },
        include: [
          { association: db.Booking.room },
          {
            association: db.Booking.student,
          },
        ],
      });
      http.setSuccess(200, "Booking retrieved", { booking });
      http.send(res);
    } catch (error) {
      let message: string = "";
      if (error instanceof Error) message = error.message;
      http.setError(400, "Unable to get Booking", {
        message: message,
      });

      http.send(res);
    }
  }
}

export default BookingController;
