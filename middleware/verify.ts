import { NextFunction, Request, Response } from "express";

import httpResponse from "../helpers/httpResponse.helper";
import db from "../database/models";
import { user } from "../config/types";

declare global {
  namespace Express {
    interface Request {
      user: user;
    }
  }
}

const http = new httpResponse();
const verifyUser = (req: Request, res: Response, next: NextFunction) => {
  const userId = req.headers.userId;
  if (userId) {
    const user = db.Users.findOne({ where: { id: userId } });
    if (user) {
      req.user = user;

      next();
    } else {
      http.setError(400, "User not found", { message: "Unable to find user" });
      http.send(res);
    }
  } else {
    http.setError(401, "Unauthorized", {
      message: "You are not authorized to perform this action",
    });
    http.send(res);
  }
};

export default verifyUser;
