import fs from "fs";
import path from "path";
import multer from "multer";

const extentions = [".png", ".jpg", ".jpeg"];
if (!fs.existsSync(path.resolve(__dirname, "../public/images")))
  fs.mkdirSync(path.resolve(__dirname, "../public/images"), {
    recursive: true,
  });

const documentFilter = (req: any, file: any, cb: Function) => {
  const ext = path.extname(file.originalname);

  if (!extentions.includes(ext)) {
    req.fileValidationError = "Only Pdf files are allowed";

    return cb(new Error("Only pdf files are allowed"));
  }

  cb(null, true);
};

const documentStorage = multer.diskStorage({
  destination: (req: any, file: any, cb: Function) => {
    cb(null, path.resolve(__dirname, "../public/images"));
  },
  filename: (req: any, file: any, cb: Function) => {
    cb(null, `${Date.now()}--${file.originalname}`);
  },
});

const imageUpdloader = multer({
  storage: documentStorage,
  fileFilter: documentFilter,
});

export default imageUpdloader;
