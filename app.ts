import express, { Express, Request, Response } from "express";
import path from "path";
import logger from "morgan";
import cookieParser from "cookie-parser";
import createHttpError from "http-errors";
import cors from "cors";

import indexRouter from "./routes/index";

const app: Express = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function (req: Request, res: Response, next: Function) {
  next(createHttpError(404));
});

// error handler
app.use(function (err: Error, req: Request, res: Response, next: Function) {
  // set locals, only providing error in development

  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(500);
  res.render("error");
});

module.exports = app;
