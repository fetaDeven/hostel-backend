const config = {
  development: {
    dialect: "sqlite",
    storage: "database.sqlite",
  },
  test: {
    username: "postgres",
    password: "0771403535",
    database: "hostel_db",
    host: "127.0.0.1",
    dialect: "postgres",

    use_env_variable: "",
  },
  production: {
    username: "postgres",
    password: "0771403535",
    database: "hostel_db",
    host: "127.0.0.1",
    dialect: "postgres",

    use_env_variable: "",
  },
};

module.exports = config;
