"use strict";

import { Sequelize, DataTypes, Model } from "sequelize";
interface FloorAttributes {
  id: number;
  name: string;
  created_by: number;
  hostel_id: number;
  gender: string;
}
module.exports = (sequelize: Sequelize) => {
  class Floor extends Model<FloorAttributes> implements FloorAttributes {
    id!: number;
    name!: string;
    created_by!: number;
    hostel_id!: number;
    gender!: string;
    static rooms: any;

    static associate(models: any) {
      // define association here
      this.rooms = models.Floors.hasMany(models.Rooms, {
        foreignKey: "floor_id",
        as: "rooms",
      });
    }
  }
  Floor.init(
    {
      name: DataTypes.STRING,
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      created_by: {
        type: DataTypes.BIGINT,
      },
      hostel_id: {
        type: DataTypes.BIGINT,
      },
      gender: {
        type: DataTypes.STRING,
      },
    },
    {
      sequelize,
      modelName: "Floors",
      tableName: "floors",
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return Floor;
};
