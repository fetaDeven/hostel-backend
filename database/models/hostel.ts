"use strict";

import { Sequelize, DataTypes, Model } from "sequelize";

interface HostelAttributes {
  id: number;
  name: string;
  created_by: number;
  location: string;
}

module.exports = (sequelize: Sequelize) => {
  class Hostel extends Model<HostelAttributes> implements HostelAttributes {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    id!: number;
    name!: string;
    created_by!: number;
    location!: string;
    hostelStudents!: any;
    static hostelStudents: any;
    static hostelImages: any;

    static associate(models: any) {
      this.hostelStudents = models.Hostels.hasMany(models.User, {
        foreignKey: "current_hostel",
        as: "students",
      });

      this.hostelImages = models.Hostels.hasMany(models.Images, {
        foreignKey: "owner_id",
        as: "hostel_images",
      });
      // define association here
    }
  }
  Hostel.init(
    {
      name: DataTypes.STRING,
      location: DataTypes.STRING,
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      created_by: {
        type: DataTypes.BIGINT,
      },
    },
    {
      sequelize,
      modelName: "Hostels",
      tableName: "hostels",
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );
  return Hostel;
};
