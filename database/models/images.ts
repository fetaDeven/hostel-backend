import { DataTypes, Model, Sequelize } from "sequelize";

interface ImageAttributes {
  id: number;
  owner_id: number;
  image: string;
  type: string;
}

module.exports = (sequelize: Sequelize) => {
  class Images extends Model<ImageAttributes> implements ImageAttributes {
    id!: number;
    owner_id!: number;
    image!: string;
    type!: string;
    static hostel: any;
    static room: any;

    static associate(models: any) {
      // define association here
      this.hostel = models.Images.belongsTo(models.Hostels, {
        foreignKey: "owner_id",
        as: "hostel",
      });
    }
  }

  Images.init(
    {
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      type: DataTypes.STRING,
      owner_id: {
        type: DataTypes.BIGINT,
      },
      image: {
        type: DataTypes.STRING,
      },
    },
    {
      sequelize,
      modelName: "Images",
      tableName: "images",
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return Images;
};
