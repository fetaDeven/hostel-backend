"use strict";

import { Sequelize, DataTypes, Model } from "sequelize";
interface UserAttributes {
  id: number;
  name: string;
  username: string;
  type: string;
  password: string;
  course: string;
  university: string;
  current_hostel: number;
}
module.exports = (sequelize: Sequelize) => {
  class User extends Model<UserAttributes> implements UserAttributes {
    username!: string;
    name!: string;
    type!: string;
    password!: string;
    id!: number; // Note that the `null assertion` `!` is required in strict mode.
    course!: string;
    university!: string;
    current_hostel!: number;
    static hostel: any;

    static associate(models: any) {
      // define association here
      this.hostel = models.User.belongsTo(models.Hostels, {
        foreignKey: "current_hostel",
        as: "students",
      });
    }
  }
  User.init(
    {
      name: DataTypes.STRING,
      university: DataTypes.STRING,
      current_hostel: DataTypes.BIGINT,
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      type: {
        type: DataTypes.STRING,
      },
      course: {
        type: DataTypes.STRING,
      },
      username: {
        type: DataTypes.STRING,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "User",
      tableName: "users",
      defaultScope: {
        attributes: { exclude: ["password"] },
      },
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );
  return User;
};
