"use strict";
import { Model, Sequelize, DataTypes } from "sequelize";

interface BookingAttributes {
  id: number;
  user_id: number;
  room_id: number;
  end_date: Date;
  created_by: number;
  paid: boolean;
  status: string;
  hostel_id: number;
}
module.exports = (sequelize: Sequelize) => {
  class Booking extends Model<BookingAttributes> implements BookingAttributes {
    id!: number;
    user_id!: number;
    room_id!: number;
    end_date!: Date;
    created_by!: number;
    paid!: boolean;
    status!: string;
    hostel_id!: number;
    static room: any;
    static hostel: any;
    static student: any;

    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models: any) {
      // define association here
      this.room = models.Booking.belongsTo(models.Rooms, {
        foreignKey: "room_id",
        as: "hostel_room",
      });

      this.hostel = models.Booking.belongsTo(models.Hostels, {
        foreignKey: "hostel_id",
        as: "hostel",
      });

      this.student = models.Booking.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "student",
      });
    }
  }
  Booking.init(
    {
      status: DataTypes.STRING,
      hostel_id: DataTypes.BIGINT,
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      user_id: {
        type: DataTypes.BIGINT,
      },
      room_id: {
        type: DataTypes.BIGINT,
      },
      end_date: {
        type: DataTypes.DATE,
      },
      created_by: {
        type: DataTypes.BIGINT,
      },
      paid: {
        type: DataTypes.BOOLEAN,
      },
    },
    {
      sequelize,
      modelName: "Booking",
      tableName: "bookings",
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );
  return Booking;
};
