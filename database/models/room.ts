"use strict";
import { Model, Sequelize, DataTypes } from "sequelize";

interface RoomAttributes {
  id: number;
  name: string;
  floor_id: number;
  booked: boolean;
  created_by: number;
  total_occupants: number;
  occupants: number;
  price: number;
}

module.exports = (sequelize: Sequelize) => {
  class Room extends Model<RoomAttributes> implements RoomAttributes {
    id!: number;
    name!: string;
    floor_id!: number;
    booked!: boolean;
    created_by!: number;
    total_occupants!: number;
    occupants!: number;
    price!: number;
    static floor: any;
    static images: any;

    static associate(models: any) {
      // define association here
      this.floor = models.Rooms.belongsTo(models.Floors, {
        foreignKey: "floor_id",
        as: "room_floor",
      });

      this.images = models.Rooms.hasMany(models.Images, {
        foreignKey: "owner_id",
        as: "room_images",
      });
    }
  }
  Room.init(
    {
      name: DataTypes.STRING,
      floor_id: DataTypes.BIGINT,
      booked: DataTypes.BOOLEAN,
      occupants: DataTypes.INTEGER,
      total_occupants: DataTypes.INTEGER,
      price: DataTypes.INTEGER,
      id: {
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
      },
      created_by: {
        type: DataTypes.BIGINT,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Rooms",
      tableName: "rooms",
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );
  return Room;
};
