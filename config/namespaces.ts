import { user } from "./types";

export namespace Express {
  export interface Request {
    user: user;
  }
}
