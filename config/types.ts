export type user = {
  id: number;
  name: string;
  username: string;
  type: string;
  password: string;
  course: string;
};

export type hostelObject = {
  id: number;
  name: string;
  created_by: number;
  location: string;
};
