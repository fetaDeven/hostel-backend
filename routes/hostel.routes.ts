import express, { Request, Response } from "express";
import HostelController from "../controllers/hostels.controller";
const hostelRouter = express.Router();

const controller = new HostelController();

hostelRouter.get("/", controller.getHostels);
hostelRouter.post("/", controller.createHostel);
hostelRouter.get("/search", controller.searchHostels);
hostelRouter.get("/:id", controller.getHostelDetails);

export default hostelRouter;
