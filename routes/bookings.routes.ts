import express from "express";
import BookingController from "../controllers/booking.controller";
const bookingRouter = express.Router();

const controller = new BookingController();

bookingRouter.get("/", controller.getBooking);
bookingRouter.get("/my-booking", controller.getMyBooking);
bookingRouter.get("/accepted-bookings", controller.getAcceptedBookings);
bookingRouter.put("/payment/:id", controller.acceptPayment);
bookingRouter.put("/accept/:id", controller.acceptBooking);
bookingRouter.post("/", controller.createBooking);
bookingRouter.put("/:id", controller.updateBooking);
bookingRouter.get("/search", controller.updateBooking);
bookingRouter.delete("/:id", controller.deleteBooking);

export default bookingRouter;
