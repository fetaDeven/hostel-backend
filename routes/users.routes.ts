import express from "express";
import UsersController from "../controllers/users.controller";

const usersRouter = express.Router();

const controller = new UsersController();

usersRouter.post("/", controller.createUser);
usersRouter.get("/", controller.getUsers);
usersRouter.get("/verify-user", controller.verifyUser);
usersRouter.post("/login", controller.loginUser);

export default usersRouter;
