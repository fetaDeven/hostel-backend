import express from "express";
import RoomsController from "../controllers/rooms.controller";
const roomsRouter = express.Router();

const controller = new RoomsController();

roomsRouter.get("/:id", controller.getRoomDetails);
roomsRouter.get("/:floorId", controller.getFloorRooms);
roomsRouter.post("/", controller.createRoom);
roomsRouter.put("/:id", controller.updateRoom);
roomsRouter.delete("/:id", controller.deleteRoom);

export default roomsRouter;
