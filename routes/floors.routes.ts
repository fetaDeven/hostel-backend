import express from "express";
import FloorsController from "../controllers/floors.controller";
const floorsRouter = express.Router();

const controller = new FloorsController();

floorsRouter.get("/:hostelId", controller.getFloors);
floorsRouter.get("/details/:id", controller.getFloorDetails);
floorsRouter.post("/", controller.createFloor);
floorsRouter.get("/search", controller.updateFloor);

export default floorsRouter;
