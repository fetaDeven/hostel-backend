import express, { Request, Response } from "express";
import HostelController from "../controllers/hostels.controller";
import imageUpdloader from "../middleware/upload";
import bookingRouter from "./bookings.routes";
import floorsRouter from "./floors.routes";
import hostelRouter from "./hostel.routes";
import roomsRouter from "./rooms.routes";
import usersRouter from "./users.routes";
const router = express.Router();

/* GET home page. */
router.get("/", function (req: Request, res: Response, next) {
  res.render("index", { title: "E Hostel App" });
});
router.post(
  "/images/:id",
  imageUpdloader.single("image"),
  HostelController.uploadImages
);
router.use("/hostels", hostelRouter);
router.use("/rooms", roomsRouter);
router.use("/users", usersRouter);
router.use("/floors", floorsRouter);
router.use("/booking", bookingRouter);

export default router;
